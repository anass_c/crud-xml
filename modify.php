<?php
/*
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

function participant($dom, $id, $sexe, $withpaper, $firstName, $lastName, $email, $email2, $paye, $devise, $montant, $author_list, $affiliation_list, $city, $country, $address, $presentation_date, $presentation_hour, $arrivalDate, $arrivalHour, $arrivalAirport, $departureDate, $departureHour, $departureAirport, $numPaper, $titlePaper, $hotelName, $hotelAddress, $hotelTel, $sePaye, $seParticipate, $seDevise, $seMontant, $accompagned, $prentship, $accName)
{
    $participant_node = $dom->createElement('participant');

    $attr_participant_id = new DOMAttr('id', $id);

    $participant_node->setAttributeNode($attr_participant_id);

    $attr_participant_sexe = new DOMAttr('sexe', $sexe);

    $participant_node->setAttributeNode($attr_participant_sexe);

    $attr_participant_paper = new DOMAttr('withpaper', $withpaper);

    $participant_node->setAttributeNode($attr_participant_paper);

    $child_node_firstName = $dom->createElement('firstName', $firstName);

    $participant_node->appendChild($child_node_firstName);

    $child_node_lastName = $dom->createElement('lastName', $lastName);

    $participant_node->appendChild($child_node_lastName);

    $child_node_email = $dom->createElement('email', $email);

    $participant_node->appendChild($child_node_email);

    // payement child
    $child_node_payement = $dom->createElement('payement');

    // payement attributes
    $attr_payement_paye = new DOMAttr('paye', $paye);

    $child_node_payement->setAttributeNode($attr_payement_paye);

    $attr_payement_devise = new DOMAttr('devise', $devise);

    $child_node_payement->setAttributeNode($attr_payement_devise);

    $attr_payement_montant = new DOMAttr('montant', $montant);

    $child_node_payement->setAttributeNode($attr_payement_montant);

    $participant_node->appendChild($child_node_payement);

    // affiliation child
    $child_node_affiliation = $dom->createElement('affiliation');

    $participant_node->appendChild($child_node_affiliation);

    // affiliation attributes
    $attr_affiliation_city = new DOMAttr('city', $city);

    $child_node_affiliation->setAttributeNode($attr_affiliation_city);

    $attr_affiliation_country = new DOMAttr('country', $country);

    $child_node_affiliation->setAttributeNode($attr_affiliation_country);

    // Address child
    $child_node_address = $dom->createElement('address', $address);

    $participant_node->appendChild($child_node_address);

    // presentation child
    $child_node_presentation = $dom->createElement('presentation');

    $participant_node->appendChild($child_node_presentation);

    // presentation attributes
    $attr_presentation_date = new DOMAttr('date', $presentation_date);

    $child_node_presentation->setAttributeNode($attr_presentation_date);

    $attr_presentation_hour = new DOMAttr('hour', $presentation_hour);

    $child_node_presentation->setAttributeNode($attr_presentation_hour);

    // travel child
    $child_node_travel = $dom->createElement('travel');

    // travel's children
    //arrival
    $grandChild_node_arrival = $dom->createElement('arrival');

    $child_node_travel->appendChild($grandChild_node_arrival);

    $attr_arrival_date = new DOMAttr('date', $arrivalDate);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_date);

    $attr_arrival_hour = new DOMAttr('hour', $arrivalHour);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_hour);

    $attr_arrival_airport = new DOMAttr('airport', $arrivalAirport);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_airport);

    $child_node_travel->appendChild($grandChild_node_arrival);

    //departure
    $grandChild_node_departure = $dom->createElement('departure');

    $child_node_travel->appendChild($grandChild_node_departure);

    $attr_departure_date = new DOMAttr('date', $departureDate);

    $grandChild_node_departure->setAttributeNode($attr_departure_date);

    $attr_departure_hour = new DOMAttr('hour', $departureHour);

    $grandChild_node_departure->setAttributeNode($attr_departure_hour);

    $attr_departure_airport = new DOMAttr('airport', $departureAirport);

    $grandChild_node_departure->setAttributeNode($attr_departure_airport);

    $child_node_travel->appendChild($grandChild_node_departure);

    $participant_node->appendChild($child_node_travel);

// paper child
    $child_node_paper = $dom->createElement('paper');

    // paper attributes
    $attr_paper_number = new DOMAttr('number', $numPaper);

    $child_node_paper->setAttributeNode($attr_paper_number);

////////////////////////////////////////////////////////////////////

    for ($i = 0; $i < count($author_list); $i++) {

        $grandChild_node_author = $dom->createElement('author');

        $child_node_paper->appendChild($grandChild_node_author);

        $ggrandChild_node_name = $dom->createElement('name', $author_list[$i]);

        $grandChild_node_author->appendChild($ggrandChild_node_name);

        $ggrandChild_node_affiliation = $dom->createElement('affiliation', $affiliation_list[$i]);

        $grandChild_node_author->appendChild($ggrandChild_node_affiliation);
    }
////////////////////////////////////////////////////////////////////
    $grandChild_node_title = $dom->createElement('title', $titlePaper);

    $child_node_paper->appendChild($grandChild_node_title);

    $participant_node->appendChild($child_node_paper);

// hotel child
    $child_node_hotel = $dom->createElement('hotel');

    $grandChild_node_name = $dom->createElement('name', $hotelName);

    $child_node_hotel->appendChild($grandChild_node_name);

    $grandChild_node_addresse = $dom->createElement('addresse', $hotelAddress);

    $child_node_hotel->appendChild($grandChild_node_addresse);

    $grandChild_node_tel = $dom->createElement('tel', $hotelTel);

    $child_node_hotel->appendChild($grandChild_node_tel);

    $participant_node->appendChild($child_node_hotel);

// social event
    $child_node_socialEvent = $dom->createElement('socialEvent');

    // social event attributes
    $attr_social_participant = new DOMAttr('participant', $seParticipate);

    $child_node_socialEvent->setAttributeNode($attr_social_participant);

    $participant_node->appendChild($child_node_socialEvent);

// payement child
    $child_node_payement_t = $dom->createElement('payement', 'chaine de caractere');

// payement attributes
    $attr_payement_paye_t = new DOMAttr('paye', $sePaye);

    $child_node_payement_t->setAttributeNode($attr_payement_paye_t);

    $attr_payement_devise_t = new DOMAttr('devise', $seDevise);

    $child_node_payement_t->setAttributeNode($attr_payement_devise_t);

    $attr_payement_montant_t = new DOMAttr('montant', $seMontant);

    $child_node_payement_t->setAttributeNode($attr_payement_montant_t);

    $child_node_socialEvent->appendChild($child_node_payement_t);

// accopagned

    $child_node_accopagned = $dom->createElement('accopagned');

// payement attributes
    $attr_accopagned_accopagned = new DOMAttr('accopagned', $accompagned);

    $child_node_accopagned->setAttributeNode($attr_accopagned_accopagned);

    $attr_accopagned_prentship = new DOMAttr('prentship', $prentship);

    $child_node_accopagned->setAttributeNode($attr_accopagned_prentship);

    $child_node_socialEvent->appendChild($child_node_accopagned);

    $grandchild_node_accopagned_name = $dom->createElement('name', $accName);

    $child_node_accopagned->appendChild($grandchild_node_accopagned_name);

    return $participant_node;
}

if (isset($_POST['Sauvegarder'])) {

    $id = $_POST['id'];

    $sexe = $_POST['sexe'];
    $withpaper = $_POST['withpaper'];
    $firstName = $_POST['fstName'];
    $lastName = $_POST['lstName'];
    $email = $_POST['email'];
    $email2 = $_POST['email2'];
    $address = $_POST['personalAddress'];
    $paye = $_POST['paye'];
    $devise = $_POST['devise'];
    $montant = $_POST['montant'];
    $city = $_POST['city'];
    $country = $_POST['country'];

    $presentation_date = $_POST['presentation_date'];
    $presentation_hour = $_POST['presentation_time'];

    $departureAirport = $_POST['departure_airport'];
    $departureDate = $_POST['departure_date'];
    $departureHour = $_POST['departure_time'];

    $arrivalAirport = $_POST['arrival_airport'];
    $arrivalDate = $_POST['arrival_date'];
    $arrivalHour = $_POST['arrival_time'];

    $titlePaper = $_POST['paper_title'];
    $numPaper = $_POST['paper_num'];

    $hotelName = $_POST['hotel_name'];
    $hotelAddress = $_POST['hotel_address'];
    $hotelTel = $_POST['hotel_tel'];

    $seParticipate = $_POST['se_participate'];
    $sePaye = $_POST['se_paye'];
    $seDevise = $_POST['se_devise'];
    $seMontant = $_POST['se_montant'];

    $accompagned = $_POST['accopagned'];
    $prentship = $_POST['prentship'];
    $accName = $_POST['accompagned_name'];

    $author_list = $_POST['author']['name'];
    $affiliation_list = $_POST['author']['affiliation'];

    $xml_file_name = 'participant.xml';

    if (file_exists('participant.xml')) {

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->preserveWhiteSpace = false;

        $dom->formatOutput = true;

        $dom->load($xml_file_name);

        $list = $dom->getElementsByTagName("participant");

        $participant_num = count($list);

        $root = $dom->getElementsByTagName("register")->item(0);

        for ($i = 0; $i < count($list); $i++) {
            $node = $list->item($i);
            if ($id == $node->getAttribute('id')) {
                $nodeToBeReplaced = $node;
                break;
            }
        }

        $participant_node = participant($dom, $id, $sexe, $withpaper, $firstName, $lastName, $email, $email2, $paye, $devise, $montant, $author_list, $affiliation_list, $city, $country, $address, $presentation_date, $presentation_hour, $arrivalDate, $arrivalHour, $arrivalAirport, $departureDate, $departureHour, $departureAirport, $numPaper, $titlePaper, $hotelName, $hotelAddress, $hotelTel, $sePaye, $seParticipate, $seDevise, $seMontant, $accompagned, $prentship, $accName);

        $root->replaceChild($participant_node, $nodeToBeReplaced);

        $dom->appendChild($root);

        if ($dom->save($xml_file_name)) {
            header('Content-type: text/xml');
            header("Location: participant.xml");
            die();
        }
    }
}

if (isset($_GET['id'])) {
    $idNodeToReplace = $_GET['id'];
    $xml_file_name = 'participant.xml';

    if (file_exists('participant.xml')) {

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->preserveWhiteSpace = false;

        $dom->formatOutput = true;

        $dom->load($xml_file_name);

        $root = $dom->documentElement;
        $list = $dom->getElementsByTagName("participant");

        for ($i = 0; $i < count($list); $i++) {
            $node = $list->item($i);
            if ($idNodeToReplace == $node->getAttribute('id')) {
                $nodeToBeReplaced = $node;
                break;
            }
        }
        $id = $nodeToBeReplaced->getAttribute('id');
        $firstName = $nodeToBeReplaced->getElementsByTagName('firstName')->item(0)->nodeValue;
        $lastName = $nodeToBeReplaced->getElementsByTagName('lastName')->item(0)->nodeValue;

        $email = $nodeToBeReplaced->getElementsByTagName('email')->item(0)->nodeValue;
        $paye = $nodeToBeReplaced->getElementsByTagName('payement')->item(0)->getAttribute('paye');
        $devise = $nodeToBeReplaced->getElementsByTagName('payement')->item(0)->getAttribute('devise');
        $montant = $nodeToBeReplaced->getElementsByTagName('payement')->item(0)->getAttribute('montant');

        $affiliation_city = $nodeToBeReplaced->getElementsByTagName('affiliation')->item(0)->getAttribute('city');
        $affiliation_country = $nodeToBeReplaced->getElementsByTagName('affiliation')->item(0)->getAttribute('country');

        ?>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    </link>
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">
    </link>
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">
    </link>
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">
    </link>
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">
    </link>
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/morris-chart/css/morris.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/graph.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/detail.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/legend.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/extensions.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/rickshaw.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/lines.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/icheck/skins/minimal/white.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS TEMPLATE - END -->
    <title>Document</title>
</head>

<body>
    <div class="page-container row-fluid">



        <form action="modify.php" method="POST">

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Les informations du participant</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" name="id" class="form-control" value="<?=$id?>">
                                <div class="form-group">
                                    <!-- <label class="form-label" for="field-1">Product Name</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="lstName" class="form-control" placeholder="Nom"
                                            required value="<?=$lastName?>">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <!-- <label class="form-label" for="field-5">Date Created</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="fstName" class="form-control" placeholder="Prenom"
                                            required value="<?=$firstName?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Status</label> -->
                                    <span class="desc"></span>
                                    <select class="form-control" name="sexe" required>
                                        <option value="H">Homme</option>
                                        <option value="F">Femme</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Vendor</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="email" class="form-control" placeholder="Email"
                                            value="<?=$email?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Vendor</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="email2" class="form-control" placeholder="Email2">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="form-label">With paper</label>
                                    <label class="form-label">Yes</label>
                                    <input type="radio" id="square-radio-1" class="skin-square-green" name="withpaper"
                                        value="Yes">
                                    <label class="form-label">No</label>
                                    <input type="radio" id="square-radio-1" class="skin-square-green" name="withpaper"
                                        value="No">
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-6">Address</label>
                                    <!-- <span class="desc">e.g. "Enter any size of text description here"</span> -->
                                    <div class="controls">
                                        <textarea class="form-control" cols="5" id="field-6" name="personalAdress"
                                            placeholder="Address" value="<?=$address?>"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Payement</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label">Paye</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="paye" required>
                                        <option value="Oui">Oui</option>
                                        <option value="Non">Non</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Devise</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="devise" required>
                                        <option value="DH">DH</option>
                                        <option value="EURO">Euro</option>
                                        <option value="DOLLAR">Dollar</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Montant</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" name="montant" min="0" step="1" class="form-control"
                                            placeholder="Montant" value="<?=$montant?>">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Affiliation</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Country</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="country" class="form-control" placeholder="Morocco"
                                            required value="<?=$affiliation_country?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">City</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="city" class="form-control" placeholder="Rabat" required
                                            value="<?=$affiliation_city?>">
                                    </div>
                                </div>

                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                <div class="text-left">
                                    <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                        value="Sauvegarder">
                                    <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                    <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                </div>
                            </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Presentation</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="presentation_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="presentation_time">
                                    </div>
                                </div>


                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                    <div class="text-left">
                                        <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                            value="Sauvegarder">
                                        <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Travel</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Airport</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="CMN" required
                                            name="departure_airport">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="departure_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="departure_time">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Airport</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="JFK" required
                                            name="arrival_airport">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="arrival_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="arrival_time">
                                    </div>
                                </div>


                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                        <div class="text-left">
                                            <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                                value="Sauvegarder">
                                            <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                            <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                        </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Paper</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Paper Title</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="title" class="form-control" placeholder="Title"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="form-label" for="field-5">paper number</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" name="paper_num" class="form-control"
                                            placeholder="Paper nunmber" required>
                                    </div>
                                </div>
                                <a><button type="button" class="btn btn-primary" id="add_row">Add</button></a>
                                <a><button type="button" class="btn btn-primary" id="delete_row">Remove</button></a>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tblSample">
                                        <thead>
                                            <tr>
                                                <th>Author</th>
                                                <th>Name</th>
                                                <th>Affiliation</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>1</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="text" name="author[name][]"
                                                                class="form-control" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="text" name="author[affiliation][]"
                                                                class="form-control" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Hotel</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Hotel Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="hotel_name" class="form-control" placeholder="Regency"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-6">Hotel Address</label>
                                    <!-- <span class="desc">e.g. "Enter any size of text description here"</span> -->
                                    <div class="controls">
                                        <textarea class="form-control" cols="5" id="field-6" name="hotel_address"
                                            placeholder="Address"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tel</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="tel" name="hotel_tel" class="form-control"
                                            placeholder="+212xxxxxxx" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Social event</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label">Participate</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="participate" required>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>

                            </div>

                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Payement</h2>

                                </header>
                                <div class="content-body">
                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <div class="form-group">
                                                <label class="form-label">Paye</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="se_paye" required>
                                                    <option value="Oui">Oui</option>
                                                    <option value="Non">Non</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Devise</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="se_devise" required>
                                                    <option value="DH">DH</option>
                                                    <option value="EURO">Euro</option>
                                                    <option value="DOLLAR">Dollar</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Montant</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="number" name="se_montant" min="0" step="1"
                                                        class="form-control" placeholder="Montant">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Accopagned</h2>

                                </header>
                                <div class="content-body">
                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <div class="form-group">
                                                <label class="form-label">Accopagned</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="accopagned" required>
                                                    <option value="Oui">Oui</option>
                                                    <option value="Non">Non</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Prentship</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="text" name="prentship" class="form-control"
                                                        placeholder="prentship">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Name</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="text" name="prentship_name" class="form-control"
                                                        placeholder="name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>

                </section>

            </div>
            <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                <div class="text-left">
                    <input type="submit" class="btn btn-primary" name="Sauvegarder" value="Sauvegarder">

                    <button type="reset" class="btn btn-warning">Réinitialiser</button>
                </div>
            </div>
        </form>


    </div>


    <!-- CORE JS FRAMEWORK - START -->
    <script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <!-- CORE JS FRAMEWORK - END -->

    <!-- CORE TEMPLATE JS - START -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->
</body>

</html>

<?php
} else {
        echo "Participant file doesn't exist";
    }
} else {
    echo "nothing to show";
}
?>
