<?php

/*
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

 if(isset($_GET['attestation'])) {

$xml = new DOMDocument("1.0","UTF-8");
$xml->load("participant.xml");

$xsl = new DOMDocument("1.0","UTF-8");
$xsl->load("attestationLatex.xsl");

$proc = new XSLTProcessor;

$proc->importStyleSheet($xsl);
$proc->setParameter('','id',$_GET['id']);

echo $proc->transformToXML($xml);
}
 ///////////////////////////////////////////////////
 if(isset($_GET['recu'])) {

$xml = new DOMDocument("1.0","UTF-8");
$xml->load("participant.xml");

$xsl = new DOMDocument("1.0","UTF-8");
$xsl->load("therealtwo.xsl");

$proc = new XSLTProcessor;

$proc->importStyleSheet($xsl);
$proc->setParameter('','id',$_GET['id']);

echo $proc->transformToXML($xml);
}
////////////////////////////////////////////////////
 if(isset($_GET['badge'])) {

$xml = new DOMDocument("1.0","UTF-8");
$xml->load("participant.xml");

$xsl = new DOMDocument("1.0","UTF-8");
$xsl->load("thereal.xsl");

$proc = new XSLTProcessor;

$proc->importStyleSheet($xsl);
$proc->setParameter('','id',$_GET['id']);

echo $proc->transformToXML($xml);
}
?>
