
<?php

/*
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

function participant($dom, $id, $sexe, $withpaper, $nom, $prenom, $email, $email2, $paye, $devise, $montant, $author_list, $affiliation_list, $city, $country, $address, $presentation_date, $presentation_hour, $arrivalDate, $arrivalHour, $arrivalAirport, $departureDate, $departureHour, $departureAirport, $numPaper, $titlePaper, $hotelName, $hotelAddress, $hotelTel, $sePaye, $seParticipate, $seDevise, $seMontant, $accompagned, $prentship, $accName)
{
    $participant_node = $dom->createElement('participant');

    $attr_participant_id = new DOMAttr('id', $id);

    $participant_node->setAttributeNode($attr_participant_id);

    $attr_participant_sexe = new DOMAttr('sexe', $sexe);

    $participant_node->setAttributeNode($attr_participant_sexe);

    $attr_participant_paper = new DOMAttr('withpaper', $withpaper);

    $participant_node->setAttributeNode($attr_participant_paper);

    $child_node_firstName = $dom->createElement('firstName', $nom);

    $participant_node->appendChild($child_node_firstName);

    $child_node_lastName = $dom->createElement('lastName', $prenom);

    $participant_node->appendChild($child_node_lastName);

    $child_node_email = $dom->createElement('email', $email);

    $participant_node->appendChild($child_node_email);

    // payement child
    $child_node_payement = $dom->createElement('payement');

    // payement attributes
    $attr_payement_paye = new DOMAttr('paye', $paye);

    $child_node_payement->setAttributeNode($attr_payement_paye);

    $attr_payement_devise = new DOMAttr('devise', $devise);

    $child_node_payement->setAttributeNode($attr_payement_devise);

    $attr_payement_montant = new DOMAttr('montant', $montant);

    $child_node_payement->setAttributeNode($attr_payement_montant);

    $participant_node->appendChild($child_node_payement);

    // affiliation child
    $child_node_affiliation = $dom->createElement('affiliation');

    $participant_node->appendChild($child_node_affiliation);

    // affiliation attributes
    $attr_affiliation_city = new DOMAttr('city', $city);

    $child_node_affiliation->setAttributeNode($attr_affiliation_city);

    $attr_affiliation_country = new DOMAttr('country', $country);

    $child_node_affiliation->setAttributeNode($attr_affiliation_country);

    // Address child
    $child_node_address = $dom->createElement('address', $address);

    $participant_node->appendChild($child_node_address);

    // presentation child
    $child_node_presentation = $dom->createElement('presentation');

    $participant_node->appendChild($child_node_presentation);

    // presentation attributes
    $attr_presentation_date = new DOMAttr('date', $presentation_date);

    $child_node_presentation->setAttributeNode($attr_presentation_date);

    $attr_presentation_hour = new DOMAttr('hour', $presentation_hour);

    $child_node_presentation->setAttributeNode($attr_presentation_hour);

    // travel child
    $child_node_travel = $dom->createElement('travel');

    // travel's children
    //arrival
    $grandChild_node_arrival = $dom->createElement('arrival');

    $child_node_travel->appendChild($grandChild_node_arrival);

    $attr_arrival_date = new DOMAttr('date', $arrivalDate);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_date);

    $attr_arrival_hour = new DOMAttr('hour', $arrivalHour);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_hour);

    $attr_arrival_airport = new DOMAttr('airport', $arrivalAirport);

    $grandChild_node_arrival->setAttributeNode($attr_arrival_airport);

    $child_node_travel->appendChild($grandChild_node_arrival);

    //departure
    $grandChild_node_departure = $dom->createElement('departure');

    $child_node_travel->appendChild($grandChild_node_departure);

    $attr_departure_date = new DOMAttr('date', $departureDate);

    $grandChild_node_departure->setAttributeNode($attr_departure_date);

    $attr_departure_hour = new DOMAttr('hour', $departureHour);

    $grandChild_node_departure->setAttributeNode($attr_departure_hour);

    $attr_departure_airport = new DOMAttr('airport', $departureAirport);

    $grandChild_node_departure->setAttributeNode($attr_departure_airport);

    $child_node_travel->appendChild($grandChild_node_departure);

    $participant_node->appendChild($child_node_travel);

// paper child
    $child_node_paper = $dom->createElement('paper');

    // paper attributes
    $attr_paper_number = new DOMAttr('number', $numPaper);

    $child_node_paper->setAttributeNode($attr_paper_number);

////////////////////////////////////////////////////////////////////

    for ($i = 0; $i < count($author_list); $i++) {

        $grandChild_node_author = $dom->createElement('author');

        $child_node_paper->appendChild($grandChild_node_author);

        $ggrandChild_node_name = $dom->createElement('name', $author_list[$i]);

        $grandChild_node_author->appendChild($ggrandChild_node_name);

        $ggrandChild_node_affiliation = $dom->createElement('affiliation', $affiliation_list[$i]);

        $grandChild_node_author->appendChild($ggrandChild_node_affiliation);
    }
////////////////////////////////////////////////////////////////////
    $grandChild_node_title = $dom->createElement('title', $titlePaper);

    $child_node_paper->appendChild($grandChild_node_title);

    $participant_node->appendChild($child_node_paper);

// hotel child
    $child_node_hotel = $dom->createElement('hotel');

    $grandChild_node_name = $dom->createElement('name', $hotelName);

    $child_node_hotel->appendChild($grandChild_node_name);

    $grandChild_node_addresse = $dom->createElement('addresse', $hotelAddress);

    $child_node_hotel->appendChild($grandChild_node_addresse);

    $grandChild_node_tel = $dom->createElement('tel', $hotelTel);

    $child_node_hotel->appendChild($grandChild_node_tel);

    $participant_node->appendChild($child_node_hotel);

// social event
    $child_node_socialEvent = $dom->createElement('socialEvent');

    // social event attributes
    $attr_social_participant = new DOMAttr('participant', $seParticipate);

    $child_node_socialEvent->setAttributeNode($attr_social_participant);

    $participant_node->appendChild($child_node_socialEvent);

// payement child
    $child_node_payement_t = $dom->createElement('payement', 'chaine de caractere');

// payement attributes
    $attr_payement_paye_t = new DOMAttr('paye', $sePaye);

    $child_node_payement_t->setAttributeNode($attr_payement_paye_t);

    $attr_payement_devise_t = new DOMAttr('devise', $seDevise);

    $child_node_payement_t->setAttributeNode($attr_payement_devise_t);

    $attr_payement_montant_t = new DOMAttr('montant', $seMontant);

    $child_node_payement_t->setAttributeNode($attr_payement_montant_t);

    $child_node_socialEvent->appendChild($child_node_payement_t);

// accopagned

    $child_node_accopagned = $dom->createElement('accopagned');

// payement attributes
    $attr_accopagned_accopagned = new DOMAttr('accopagned', $accompagned);

    $child_node_accopagned->setAttributeNode($attr_accopagned_accopagned);

    $attr_accopagned_prentship = new DOMAttr('prentship', $prentship);

    $child_node_accopagned->setAttributeNode($attr_accopagned_prentship);

    $child_node_socialEvent->appendChild($child_node_accopagned);

    $grandchild_node_accopagned_name = $dom->createElement('name', $accName);

    $child_node_accopagned->appendChild($grandchild_node_accopagned_name);

    return $participant_node;
}

// delete the file
// $myFile = "participant.xml";
// unlink($myFile);

$sexe = $_POST['sexe'];
$withpaper = $_POST['withpaper'];
$firstName = $_POST['fstName'];
$lastName = $_POST['lstName'];
$email = $_POST['email'];
$email2 = $_POST['email2'];
$address = $_POST['personalAddress'];
$paye = $_POST['paye'];
$devise = $_POST['devise'];
$montant = $_POST['montant'];
$city = $_POST['city'];
$country = $_POST['country'];

$presentation_date = $_POST['presentation_date'];
$presentation_hour = $_POST['presentation_time'];


$departureAirport = $_POST['departure_airport'];
$departureDate = $_POST['departure_date'];
$departureHour = $_POST['departure_time'];

$arrivalAirport = $_POST['arrival_airport'];
$arrivalDate = $_POST['arrival_date'];
$arrivalHour = $_POST['arrival_time'];

$titlePaper = $_POST['paper_title'];
$numPaper = $_POST['paper_num'];

$hotelName = $_POST['hotel_name'];
$hotelAddress = $_POST['hotel_address'];
$hotelTel = $_POST['hotel_tel'];

$seParticipate = $_POST['se_participate'];
$sePaye = $_POST['se_paye'];
$seDevise = $_POST['se_devise'];
$seMontant = $_POST['se_montant'];

$accompagned = $_POST['accopagned'];
$prentship = $_POST['prentship'];
$accName = $_POST['accompagned_name'];

$author_list = $_POST['author']['name'];
$affiliation_list = $_POST['author']['affiliation'];

$xml_file_name = 'participant.xml';

if (file_exists('participant.xml')) {

    $dom = new DOMDocument();

    $dom->encoding = 'utf-8';

    $dom->xmlVersion = '1.0';

    $dom->preserveWhiteSpace = false;

    $dom->formatOutput = true;

    $dom->load($xml_file_name);

    if (!($dom->firstChild instanceof \DOMProcessingInstruction)) {
        //     $dom->removeChild($dom->firstChild);
        $xslt = $dom->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="displayParticipants.xsl"');

        //adding it to the xml
        $dom->appendChild($xslt);
    }

    $list = $dom->getElementsByTagName("participant");

    $participant_num = count($list);

    if ($participant_num == 0) {
        $lastId = 0;
    } else {
        $lastParticipant = $list->item($participant_num - 1);

        $lastId = $lastParticipant->getAttribute('id');
    }

    $root = $dom->getElementsByTagName("register")->item(0);
    ////////////////////////////////////////////////////////////

    $participant_node = participant($dom, $lastId + 1, $sexe, $withpaper, $firstName, $lastName, $email, $email2, $paye, $devise, $montant, $author_list, $affiliation_list, $city, $country, $address, $presentation_date, $presentation_hour, $arrivalDate, $arrivalHour, $arrivalAirport, $departureDate, $departureHour, $departureAirport, $numPaper, $titlePaper, $hotelName, $hotelAddress, $hotelTel, $sePaye, $seParticipate, $seDevise, $seMontant, $accompagned, $prentship, $accName);

    $root->appendChild($participant_node);
///////////////////////////////////////////////////////////////////////////////////////////
    $dom->appendChild($root);

    if ($dom->save($xml_file_name)) {

        $to = $email;
        $subject = 'Conference';
        $message = 'hello';
        $headers = 'From: webmaster@example.com' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        header('Content-type: text/xml');
        header("Location: participant.xml");
    }

    // var_dump($root);
    // $xml->appendChild($participant_node);

    // $root->appendChild($participant_node);

    // echo "<a href=\"$xml_file_name\">$xml_file_name</a> has been successfully updated";
    // print_r($list);
} else {

    // in case there is no file called participant.xml
    $dom = new DOMDocument();

    $dom->encoding = 'utf-8';

    $dom->xmlVersion = '1.0';

    $dom->formatOutput = true;

    if (!($dom->firstChild instanceof \DOMProcessingInstruction)) {
        //     $dom->removeChild($dom->firstChild);
        $xslt = $dom->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="displayParticipants.xsl"');

        //adding it to the xml
        $dom->appendChild($xslt);
    }

    $root = $dom->createElement('register');

    $attr1_register = new DOMAttr('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');

    $root->setAttributeNode($attr1_register);

    $attr2_register = new DOMAttr('xsi:noNamespaceSchemaLocation', 'shemaFile2.xsd');

    $root->setAttributeNode($attr2_register);

    $participant_node = participant($dom, 1, $sexe, $withpaper, $firstName, $lastName, $email, $email2, $paye, $devise, $montant, $author_list, $affiliation_list, $city, $country, $address, $presentation_date, $presentation_hour, $arrivalDate, $arrivalHour, $arrivalAirport, $departureDate, $departureHour, $departureAirport, $numPaper, $titlePaper, $hotelName, $hotelAddress, $hotelTel, $sePaye, $seParticipate, $seDevise, $seMontant, $accompagned, $prentship, $accName);

    $root->appendChild($participant_node);

    $dom->appendChild($root);

    if ($dom->save($xml_file_name)) {
        header('Content-type: text/xml');
        header("Location: participant.xml");
    }

    // echo "<a href=\"$xml_file_name\">$xml_file_name</a> has been successfully created";
}
?>
