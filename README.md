L'equipe:
	- Anass Chaaraoui
	- Souad Laariche

- Prérequis (linux):
	- apache2
	- php

- unzip le fichier dans votre repertoire
- acceder a ce dossier a partir le terminal

- Taper les commandes suivantes; 

	- sudo service apache2 start
	- php -S localhost:8000

- Dans votre naviguateur taper cette url:
	
	- localhost:8000/newForm.php

- A partir cette formulaire vous pouvez enregistrer vos donnees dans le fichier qui va etre creer par l'application si il n'exist pas sinon il va les ajouter.

- vous pouvez consulter tous les inscriptions, les modifier, les supprimer et telecharger les attestations, badges et recu de paiment, vous pouvez egalement chercher quelque'un via cette url:
	
	- localhost:8000/participant.xml

