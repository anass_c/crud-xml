<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
<xsl:output method="text" encoding="UTF-8"/>
	<xsl:template match="/register">
	\documentclass{article}
	\usepackage[utf8]{inputenc}


	\begin{document}

	\section*{Recu de paiement}

	\begin{tabular}
	{|p{1.7in}|p{1.5in}|} \hline 
	Payé Par \newline \newline  <xsl:value-of select="participant[@id=$id]/lastName"/> ,  <xsl:value-of select="participant[@id=$id]/firstName"/> \newline \newline   &amp;Num de TVA \newline \newline SDFGHJ456 \\ \hline 
	Montant Total \newline <xsl:value-of select="participant[@id=$id]/payement/@montant"/>  <xsl:value-of select="participant[@id=$id]/payement/@devise "/> &amp; Statut : \newline <xsl:value-of select="participant[@id=$id]/payement/@paye"/>   \\ \hline 
	Lieu et date \newline <xsl:value-of select="participant[@id=$id]/affiliation/@city"/> : <xsl:value-of select="participant[@id=$id]/presentation/@date"/> ,
	  &amp;Tompon signatur\newline \newline \newline\newline \newline  \\ \hline 
	\end{tabular}

	\end{document}

    </xsl:template>
</xsl:stylesheet>
