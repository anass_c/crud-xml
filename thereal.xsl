<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
<xsl:output method="text" encoding="UTF-8"/>
	<xsl:template match="/register">
	\documentclass{article}
	\usepackage[utf8]{inputenc}
	\usepackage[french,english]{babel}
	\usepackage{pst-all} 
	\usepackage{graphicx} 

	\documentclass{minimal}
	\usepackage{hyperref}
	\usepackage{color}

	\begin{document}
	\psset{unit=2in,linewidth=5pt} 
	\rput(3,18){\includegraphics[width=8cm]{logofsr.png}}
	\\
	\\
	\\
	\\
	\\
	\\
	\\
	\\
	\\
	\\
	\hline
	\begin{center}
    	\section*{Certificate of Participation} 
	\end{center}\begin{Form} 
	\begin{center}
    	\textbf{This is to certify that}
	\end{center}
	\begin{center}
    	\textbf{<xsl:value-of select="participant[@id=$id]/firstName"/> }
    	\textbf{<xsl:value-of select="participant[@id=$id]/lastName"/>}
	\end{center}
	\\\\
	\begin{center}
    	\textbf{Presented a paper titled} 
	\end{center}
	\begin{center}
    	\section*{Bla Bla Bla Bla Ba Bla Bla Bla Bla Bla Ba Bla Bla Bla Bla Bla Ba Bla Bla Bla Bla Bla Ba Bla Bla Bla Bla Bla ...} 
	\end{center}
	\begin{center}
    	\textbf{at the University of Mohammed 5} 
	\end{center}
	\begin{center}
    	\textbf{(FSR 2019)} 
	\end{center}
	\begin{center}
    	\textbf{Participating \textcolor{red}{<xsl:value-of select="participant[@id=$id]/presentation/@date"/>}} 
	\end{center}
	 \\
	 \\
	 \\
	 \\
	 \begin{center}
    	\textbf{Signed} 
	\end{center}

	\end{Form}


	\end{document}

    </xsl:template>
</xsl:stylesheet>
