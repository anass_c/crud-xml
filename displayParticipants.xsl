<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<?xml version="1.0" encoding="UTF-8"?>
<html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    </link>
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">
    </link>
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">
    </link>
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">
    </link>
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">
    </link>
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/morris-chart/css/morris.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/graph.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/detail.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/legend.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/extensions.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/rickshaw.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/lines.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/icheck/skins/minimal/white.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS TEMPLATE - END -->

</head>

<body style="font-family:Arial;font-size:12pt">
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
        <a href="newForm.php"><button type="button" class="btn btn-primary">Ajouter participant</button></a>
        <form action="search.php" method="GET">
            <div class="form-group">
                <!-- <label class="form-label" for="field-1">Product Name</label> -->
                <span class="desc"></span>
                <div class="text-left">
                    <input type="text" name="id" class="form-control" placeholder="search by name or id"></input>
                    <input type="submit" class="btn btn-primary" name="searh" value="search"></input>
                </div>
            </div>
        </form>
        <section class="box ">
            <div class="content-body">

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="register/participant">
                                        <tr>
                                            <th scope="row">
                                                <xsl:value-of select="@id" />
                                            </th>
                                            <td>
                                                <xsl:value-of select="lastName" />
                                            </td>
                                            <td>
                                                <xsl:value-of select="firstName" />
                                            </td>
                                            <td>
                                                <xsl:value-of select="email" />
                                            </td>
                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="remove.php?id={@id}">
                                                    <img src="assets/images/del.png" width="30" height="30" />
                                                </a>
                                            </td>
                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="modify.php?id={@id}">
                                                    <img src="assets/images/modify.png" width="30" height="30" />
                                                </a>
                                            </td>

                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="viewForm.php?id={@id}">
                                                    <img src="assets/images/view.png" width="30" height="30" />
                                                </a>
                                            </td>

                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="thereal.php?id={@id}&amp;recu=">
                                                    <img src="assets/images/download.png" width="30" height="30" />
                                                    <p>telecharger<br/>recu</p>
                                                </a>
                                            </td>
                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="thereal.php?id={@id}&amp;attestation=">
                                                    <img src="assets/images/download.png" width="30" height="30" />
                                                    <p>telecharger<br/>attestation</p>
                                                </a>
                                            </td>
                                            <td style="text-align: center;padding-top: 25px;">
                                                <a href="thereal.php?id={@id}&amp;badge=">
                                                    <img src="assets/images/download.png" width="30" height="30" />
                                                    <p>Badge de participation<br/> à la conference</p>
                                                </a>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>

</html>
