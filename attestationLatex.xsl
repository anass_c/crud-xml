<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text"/>
  <xsl:template match="register">
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[french,english]{babel}
\usepackage{pst-all} 
\usepackage{graphicx} 

\documentclass{minimal}
\usepackage{hyperref}
\usepackage{color}

\begin{document}
\psset{unit=2in,linewidth=8pt} 
\rput(3,18){\includegraphics[width=8cm]{download.png}}
\\
\\
\\
\\
\\
\section*{ATTESTATION DE PARTICIPATION À LA CONFÉRENCE}
\begin{Form}
\TextField[name=name,width=9cm,charsize=15pt]{}\TextField[name=name,width=3cm,charsize=15pt]{Rabat le 29 février 2019}\\\\\\
\TextField[name=name,width=0.1cm,charsize=20pt]{Je soussignée : \textcolor{red}{\textbf{<xsl:value-of select="participant[@id=$id]/lastName"/>}} }
\TextField[name=name,width=0.1cm,charsize=20pt]{\textbf{ \textcolor{red}{<xsl:value-of select="participant[@id=$id]/firstName"/>}}}\\\\\\ \TextField[name=name,width=0.2cm,charsize=20pt]{Certifie que Madame, Monsieur, Mademoiselle}
\TextField[name=name,width=2cm,charsize=20pt]{Participe  le: \textcolor{red}{<xsl:value-of select="participant[@id=$id]/presentation/@date"/>} }\\\\\\
\TextField[name=name,width=2cm,charsize=20]{{\textbf{A la conférence / au séminaire intitulé(e)} }}\\\\\\\\\\\\
\TextField[name=name,width=0.1cm,charsize=20pt]{Délivrée pour servir et valoir ce que de droit }\\\\
 \\
 \\
 \\
 \\
 \TextField[name=name,width=9cm,charsize=20pt]{ }
 \TextField[name=name,width=2cm,charsize=20pt]{signature :  }

\end{Form}


\end{document}



</xsl:template>
</xsl:stylesheet>



























