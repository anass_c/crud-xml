<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    </link>
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">
    </link>
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">
    </link>
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">
    </link>
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">
    </link>
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/datepicker/css/datepicker.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/morris-chart/css/morris.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/graph.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/detail.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/legend.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/extensions.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/rickshaw.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/lines.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/icheck/skins/minimal/white.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS TEMPLATE - END -->
    <title>Document</title>
</head>

<body>
    <div class="page-container row-fluid">


        <form action="xml.php" method="POST">

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Les informations du participant</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <!-- <label class="form-label" for="field-1">Product Name</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="lstName" class="form-control" placeholder="Nom"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <!-- <label class="form-label" for="field-5">Date Created</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="fstName" class="form-control" placeholder="Prenom"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Status</label> -->
                                    <span class="desc"></span>
                                    <select class="form-control" name="sexe" required>
                                        <option value="H">Homme</option>
                                        <option value="F">Femme</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Vendor</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="email" class="form-control" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- <label class="form-label">Vendor</label> -->
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="email2" class="form-control" placeholder="Email2">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="form-label">With paper</label>
                                    <label class="form-label">Yes</label>
                                    <input type="radio" id="square-radio-1" class="skin-square-green" name="withpaper"
                                        value="Yes">
                                    <label class="form-label">No</label>
                                    <input type="radio" id="square-radio-1" class="skin-square-green" name="withpaper"
                                        value="No">
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-6">Address</label>
                                    <!-- <span class="desc">e.g. "Enter any size of text description here"</span> -->
                                    <div class="controls">
                                        <textarea class="form-control" cols="5" id="field-6" name="personalAddress"
                                            placeholder="Address"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Payement</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label">Paye</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="paye" required>
                                        <option value="Oui">Oui</option>
                                        <option value="Non">Non</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Devise</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="devise" required>
                                        <option value="DH">DH</option>
                                        <option value="EURO">Euro</option>
                                        <option value="DOLLAR">Dollar</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Montant</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" name="montant" min="0" step="1" class="form-control"
                                            placeholder="Montant">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Affiliation</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Country</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="country" class="form-control" placeholder="Morocco"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">City</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="city" class="form-control" placeholder="Rabat"
                                            required>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                            <div class="text-left">
                                                <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                                    value="Sauvegarder">
                                                <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                                <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                            </div>
                                        </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Presentation</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="presentation_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="presentation_time">
                                    </div>
                                </div>


                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                                <div class="text-left">
                                                    <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                                        value="Sauvegarder">
                                                    <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                                    <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                                </div>
                                            </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Travel</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">


                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Airport</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="CMN" required
                                            name="departure_airport">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="departure_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Departure Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="departure_time">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Airport</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" value="JFK" required
                                            name="arrival_airport">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Date</label>
                                    <div class="controls">
                                        <input type="text" class="form-control datepicker" data-format="dd-mm-yyyy"
                                            value="07-10-2019" required name="arrival_date">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Arrival Time</label>
                                    <div class="controls">
                                        <input type="text" class="form-control timepicker" placeholder="13:00" required
                                            name="arrival_time">
                                    </div>
                                </div>


                                <!-- <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                                                    <div class="text-left">
                                                        <input type="submit" class="btn btn-primary" name="Sauvegarder"
                                                            value="Sauvegarder">
                                                        <!-- <button type="submit" name="Sauvegarder" value="Sauvegarder" class="btn btn-primary">Sauvegarder</button> ->
                                                        <button type="reset" class="btn btn-warning">Réinitialiser</button>
                                                    </div>
                                                </div> -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Paper</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Paper Title</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="paper_title" class="form-control" placeholder="Title"
                                            required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="form-label" for="field-5">paper number</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" name="paper_num" class="form-control"
                                            placeholder="Paper nunmber" required>
                                    </div>
                                </div>
                                <a><button type="button" class="btn btn-primary" id="add_row">Add</button></a>
                                <a><button type="button" class="btn btn-primary" id="delete_row">Remove</button></a>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tblSample">
                                        <thead>
                                            <tr>
                                                <th>Author</th>
                                                <th>Name</th>
                                                <th>Affiliation</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>1</td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="text" name="author[name][]"
                                                                class="form-control" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <input type="text" name="author[affiliation][]"
                                                                class="form-control" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Hotel</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Hotel Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="hotel_name" class="form-control" placeholder="Regency"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-6">Hotel Address</label>
                                    <!-- <span class="desc">e.g. "Enter any size of text description here"</span> -->
                                    <div class="controls">
                                        <textarea class="form-control" cols="5" id="field-6" name="hotel_address"
                                            placeholder="Address"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Tel</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="tel" name="hotel_tel" class="form-control"
                                            placeholder="+212xxxxxxx" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <section class="box ">
                    <header class="panel_header">
                        <h2 class="title pull-left">Social event</h2>

                    </header>
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <label class="form-label">Participate</label>
                                    <span class="desc"></span>
                                    <select class="form-control" name="se_participate" required>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>

                            </div>

                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Payement</h2>

                                </header>
                                <div class="content-body">
                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <div class="form-group">
                                                <label class="form-label">Paye</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="se_paye" required>
                                                    <option value="Oui">Oui</option>
                                                    <option value="Non">Non</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label">Devise</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="se_devise" required>
                                                    <option value="DH">DH</option>
                                                    <option value="EURO">Euro</option>
                                                    <option value="DOLLAR">Dollar</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Montant</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="number" name="se_montant" min="0" step="1"
                                                        class="form-control" placeholder="Montant">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title pull-left">Accopagned</h2>

                                </header>
                                <div class="content-body">
                                    <div class="row">

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <div class="form-group">
                                                <label class="form-label">Accopagned</label>
                                                <span class="desc"></span>
                                                <select class="form-control" name="accopagned" required>
                                                    <option value="Oui">Oui</option>
                                                    <option value="Non">Non</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Prentship</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="text" name="prentship" class="form-control"
                                                        placeholder="prentship">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Name</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input type="text" name="accompagned_name" class="form-control"
                                                        placeholder="name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>

                </section>

            </div>
            <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 padding-bottom-30">
                <div class="text-left">
                    <input type="submit" class="btn btn-primary" name="Sauvegarder" value="Sauvegarder">

                    <button type="reset" class="btn btn-warning">Réinitialiser</button>
                </div>
            </div>
        </form>


    </div>



    <!-- CORE JS FRAMEWORK - START -->
    <script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
    <!-- CORE JS FRAMEWORK - END -->

    <script src="assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>

    <script src="assets/plugins/datepicker/js/datepicker.js" type="text/javascript"></script>

    <!-- CORE TEMPLATE JS - START -->
    <script src="assets/js/scripts.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS - END -->

    <script type="text/javascript">
        window.onload = function () {

            $(document).ready(function () {
                var i = 1;
                $("#add_row").click(function () {

                    //   $('#tblSample').append('<tr id="row' + (i) + '"><td>' + (i + 1) + '</td><td><input type="text" name="author[name][]" size="40" /></td><td><input type="text" name="author[affiliation][]" size="40" /></td></tr>');
                    $('#tblSample').append('<tr id="row' + (i) + '"><td>' + (i + 1) + '</td><td><div class="form-group"><div class="controls"><input type="text" name="author[name][]" class="form-control" placeholder="" required></div></div></td><td><div class="form-group"><div class="controls"><input type="text"name="author[affiliation][]"class="form-control" placeholder="" required></div></div></td></tr>');
                    i++;
                });

                $("#delete_row").click(function () {
                    if (i > 1) {
                        $("#row" + (i - 1)).remove();
                        i--;
                    }
                });

            });
        }

    </script>
</body>

</html>
