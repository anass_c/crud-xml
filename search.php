<!--
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
<head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
    </link>
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">
    </link>
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">
    </link>
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">
    </link>
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">
    </link>
    <!-- For iPad Retina display -->




    <!-- CORE CSS FRAMEWORK - START -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    <link href="assets/plugins/morris-chart/css/morris.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jquery-ui/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/graph.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/detail.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/legend.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/extensions.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/rickshaw.min.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/rickshaw-chart/css/lines.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <link href="assets/plugins/icheck/skins/minimal/white.css" rel="stylesheet" type="text/css" media="screen">
    </link>
    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE CSS TEMPLATE - START -->
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    </link>
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
    </link>
    <!-- CORE CSS TEMPLATE - END -->

</head>

<body style="font-family:Arial;font-size:12pt">
    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">


        <section class="box ">
            <div class="content-body">

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php

if (isset($_GET['id'])) {
    $idNode = $_GET['id'];
    $xml_file_name = 'participant.xml';

    if (file_exists('participant.xml')) {

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->preserveWhiteSpace = false;

        $dom->formatOutput = true;

        $dom->load($xml_file_name);

        $root = $dom->documentElement;
        $list = $dom->getElementsByTagName("participant");

        $participant_num = count($list);

        $array = [];

        for ($i = 0; $i < count($list); $i++) {
            $node = $list->item($i);
            $nom = $node->getElementsByTagName('lastName')->item(0)->nodeValue;
            $id = $node->getAttribute('id');
            if ($idNode == $id || $idNode == $nom) {
                echo "<tr>
                        <th scope=\"row\">$id
                        </th>
                        <td>
                        " . $node->getElementsByTagName('lastName')->item(0)->nodeValue . "
                        </td>
                        <td>
                        " . $node->getElementsByTagName('firstName')->item(0)->nodeValue . "
                        </td>
                        <td>
                        " . $node->getElementsByTagName('email')->item(0)->nodeValue . "
                        </td>
                        <td style=\"text-align: center;padding-top: 25px;\">
                            <a href=\"remove.php?id=" . $id . "\">
                                <img src=\"assets/images/del.png\" width=\"30\" height=\"30\" />
                            </a>
                        </td>
                        <td style=\"text-align: center;padding-top: 25px;\">
                            <a href=\"modify.php?id=$id\">
                                <img src=\"assets/images/modify.png\" width=\"30\" height=\"30\" />
                            </a>
                        </td>

                        <td style=\"text-align: center;padding-top: 25px;\">
                            <a href=\"viewForm.php?id=$id\">
                                <img src=\"assets/images/view.png\" width=\"30\" height=\"30\" />
                            </a>
                        </td>
                        <td style=\"text-align: center;padding-top: 25px;\">
                                                <a href=\"thereal.php?id=$id&recu=\">
                                                    <img src=\"assets/images/download.png\" width=\"30\" height=\"30\" />
                                                    <p>telecharger<br/>recu</p>
                                                </a>
                                            </td>
                                            <td style=\"text-align: center;padding-top: 25px;\">
                                                <a href=\"thereal.php?id=$id&attestation=\">
                                                    <img src=\"assets/images/download.png\" width=\"30\" height=\"30\" />
                                                    <p>telecharger<br/>attestation</p>
                                                </a>
                                            </td>
                                            <td style=\"text-align: center;padding-top: 25px;\">
                                                <a href=\"thereal.php?id=$id&badge=\">
                                                    <img src=\"assets/images/download.png\" width=\"30\" height=\"30\" />
                                                    <p>Badge de participation<br/> à la conference</p>
                                                </a>
                                            </td>
                    </tr>
                ";
            }
        }?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>

</html>
<?php
// if ($removedChild == null) {
        //     echo "The child has not been removed <br/>";
        // } else {
        //     // echo "The child has been removed successfully <br/>";
        //     if ($dom->save($xml_file_name)) {
        //         header('Content-type: text/xml');
        //         header("Location: participant.xml");
        //         die();
        //     }

        // }
        // // echo "<a href=\"$xml_file_name\">$xml_file_name</a>";
    }
} else {
    echo "nothing to show";
}
?>
