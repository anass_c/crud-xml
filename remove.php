<?php
/*
MIT License

Copyright (c) 2019 Chaaraoui anass

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

if (isset($_GET['id'])) {
    $idNodeToRemove = $_GET['id'];
    $xml_file_name = 'participant.xml';

    if (file_exists('participant.xml')) {

        $dom = new DOMDocument();

        $dom->encoding = 'utf-8';

        $dom->xmlVersion = '1.0';

        $dom->preserveWhiteSpace = false;

        $dom->formatOutput = true;

        $dom->load($xml_file_name);

        $root = $dom->documentElement;
        $list = $dom->getElementsByTagName("participant");

        $participant_num = count($list);

        for ($i = 0; $i < count($list); $i++) {
            $node = $list->item($i);
            if ($idNodeToRemove == $node->getAttribute('id')) {
                // break;
                $removedChild = $root->removeChild($node);
            }
        }
        if ($removedChild == null) {
            echo "The child has not been removed <br/>";
        } else {
            // echo "The child has been removed successfully <br/>";
            if ($dom->save($xml_file_name)) {
                header('Content-type: text/xml');
                // header("Location: participant.xml");
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                die();
            }
            
        }
        // echo "<a href=\"$xml_file_name\">$xml_file_name</a>";
    }
} else {
    echo "nothing to show";
}
